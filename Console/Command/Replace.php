<?php
/**
 * Copyright ©  kowal sp. z o.o. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\ReplaceSkuFromCsv\Console\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Magento\Framework\App\ResourceConnection;

class Replace extends Command
{

    const FILE = "name";
    const PROD = "test";

    public $store_id = 0;
    public $test = false;

    /**
     * @var ResourceConnection
     */
    protected $resourceConnection;

    public function __construct(
        ResourceConnection $resourceConnection
    )
    {
        $this->resourceConnection = $resourceConnection;
    }


    /**
     * {@inheritdoc}
     */
    protected function execute(
        InputInterface  $input,
        OutputInterface $output
    )
    {
        $csv_file = $input->getArgument(self::FILE);
        $this->test = $input->getOption(self::PROD);
        if ($this->test != "-p") $output->writeln("Trub testowy - nic ne jest zapisywane! Aby uruchomic tryb produkcyjny dodaj parametr -p");
        $tables = $this->tablesName();
        if (!$csv_file) {
            $output->writeln("Proszę podać nazwę pliku CSV !");
            return;
        }

        $skus = $this->readCSV($csv_file);
        $connection = $this->_getConnection('core_write');
        foreach ($skus as $old_sku => $new_sku) {
            foreach ($tables as $table) {
                $this->replaceSku($old_sku, $new_sku, $table, $connection);
                $output->writeln("Replace  " . $old_sku . " on " . $new_sku);
            }
        }
        $output->writeln("Odczyt " . $csv_file);
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName("kowal_replaceskufromcsv:replace");
        $this->setDescription("Replace SKU from CSV ");
        $this->setDefinition([
            new InputArgument(self::FILE, InputArgument::OPTIONAL, "CSV File Name in root Magento folder"),
            new InputOption(self::PROD, "-p", InputOption::VALUE_NONE, "Tryb produkcyjny")
        ]);
        parent::configure();
    }

    private function readCSV($file_)
    {
        $file = fopen($file_, 'r');
        $skus = [];
        while (($line = fgetcsv($file)) !== FALSE) {
            if (isset($line[0]) && isset($line[1]) && !empty($line[0]) && !empty($line[1])) $skus[trim($line[0])] = trim($line[1]);
        }
        fclose($file);
        return $skus;
    }

    private function replaceSku($sku, $newSku, $table, $connection)
    {
        $sql = "UPDATE " . $this->_getTableName($table) . " cpe SET cpe.sku = ? WHERE cpe.sku = ?";
        if ($this->test == "-p") $connection->query($sql, array($newSku, $sku));
    }

    /**
     * SELECT TABLE_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMN_NAME LIKE 'sku' and TABLE_SCHEMA= "lampym2_prod2";
     */
    private function tablesName()
    {
        return ['catalog_product_entity', 'catalog_product_option', 'catalog_product_option_type_value', 'quote_address_item', 'quote_item', 'sales_creditmemo_item', 'sales_invoice_item', 'sales_order_item' . 'sales_shipment_item'];
    }


    /**
     * @return mixed
     */
    private function _getReadConnection()
    {
        return $this->_getConnection('core_read');
    }


    /**
     * @param string $type
     * @return mixed
     */
    private function _getConnection($type = 'core_read')
    {
        return $this->resourceConnection->getConnection($type);
    }

    /**
     * @param $tableName
     * @return mixed
     */
    private function _getTableName($tableName)
    {
        return $this->resourceConnection->getTableName($tableName);
    }

}

